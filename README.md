# Bounce focus

This repository is a test at creating a bounce hover->focus->active transition.

Most interesting stuff is in: [src/style/focusable.scss](./src/style/focusable.scss)

## Testing

```sh
git clone https://gitlab.com/monsieurman/bounce-focus.git
cd bounce-focus
npm i
npm start
```
